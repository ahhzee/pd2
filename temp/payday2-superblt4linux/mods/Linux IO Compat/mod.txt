{
    "name": "Linux IO Compat",
    "description": "This should fix pretty much all problems with SystemFS:* and file.* on linux",
    "author": "BangL",
    "contact": "id/BangL",
    "version": "0.2",
    "blt_version": 2,
    "color" : "0 0.47 1",
    "priority" : 1000,
    "updates" : [
        {
            "identifier" : "lioc"
        }
    ],
    "pre_hooks" : [
        {
            "hook_id" : "core/lib/system/coresystem",
            "script_path" : "coresystem.lua"
        }
    ]
}