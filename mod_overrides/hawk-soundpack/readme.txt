Hawk's near complete Sound Pack



Installation Directions: Place/Create a folder in your PAYDAY 2/assets/mod_overrides/Hawks_soundpack/soundbanks, then drop all of the files within this zip file into the soundbanks folder.
To reiterate you need to create 2 folders, whatever sound pack lable that you choose to use then inside of that folder create the soundbanks folder.
Streamed folder also goes into soundbanks folder.

If there are some sounds that you simply do not like, remove them by name, the reload and weapon manipulation sounds however are in one .bnk file so if you do not like them remove the regular_weapon_sfx.bnk file.



Sound Credits: 
Firearms Source mod
Insurgency 
Red Orchestra: Ostfront 41-45
Red Orchestra 2
Call of Juarez Gunslinger
Killing Floor 1 & 2
Battlefield 3 & 4 
Shadow Warrior (the remake) 
Medal of Honor Warfighters
Rainbow Six Vegas 2 
Doom (2016)
Gunshots recorded by Hashimoto Soda
Day of Infamy
Forgotten Hope 2

Big thanks to Colt .45 Killer and Quas for putting together tools that made this happen.

If you would like to use any of the content within this pack please contact me and we can work something out or at the very least inform me.