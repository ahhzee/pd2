
-- the blt4l implementation of 2.0rev1 uses the first param, not the second one.
if SystemFS:exists("/jhfjkhlsdfjlsdnkf/file_that_doesnt_exist_for_sure") then
	local real_exists = SystemFS.exists
	function SystemFS:exists(path)
		return real_exists(path)
	end
end

if file.MoveDirectory == nil then
	function file.MoveDirectory(source, target)
		os.execute("mv \"" .. Application:nice_path(source) .. "\" \"" .. Application:nice_path(target) .. "\"")
	end
end

if SystemFS.rename_file == nil then
	function SystemFS:rename_file(source, target)
		file.MoveDirectory(source, target)
	end
end

if SystemFS.copy_file == nil then
	function SystemFS:copy_file(source, target)
		os.execute("cp -a \"" .. Application:nice_path(source) .. "\" \"" .. Application:nice_path(target) .. "\"")
	end
end

if SystemFS.copy_files_async == nil then
	function SystemFS:copy_files_async(source, target)
		do
			SystemFS:copy_file(source, target)
		end
	end
end

if SystemFS.make_dir == nil then
	function SystemFS:make_dir(path)
		return file.CreateDirectory(Application:nice_path(path))
	end
end

if SystemFS.open == nil then
	function SystemFS:open(path, flags)
		return io.open(Application:nice_path(path), flags)
	end
end

if SystemFS.close == nil then
	function SystemFS:close(path)
		return io.close(Application:nice_path(path))
	end
end

if SystemFS.list == nil then
	function SystemFS:list(path, dirs)
		if dirs then
			return file.GetDirectories(Application:nice_path(path))
		else
			return file.GetFiles(Application:nice_path(path))
		end
	end
end

if SystemFS.delete_file == nil then
	function SystemFS:delete_file(path)
		os.execute("rm -r \"" .. Application:nice_path(path) .. "\"")
	end
end

if SystemFS.is_dir == nil then
	-- todo
end

if SystemFS.parse_xml == nil then
	-- todo
end
