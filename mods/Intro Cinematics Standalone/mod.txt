{
	"name": "Intro Cinematics Standalone",
	"description": "Adds intro cinematics to most heists",
	"author": "ThuverX",
	"contact": "ThuverX",
	"version": "2",
	"priority": 0,
	"hooks": [
		{
			"hook_id": "lib/setups/setup",
			"script_path": "lua/Core.lua"
		},
		{
			"hook_id": "core/lib/setups/coresetup",
			"script_path": "lua/Core.lua"
		},
		{
			"hook_id": "lib/states/ingamewaitingforplayers",
			"script_path": "lua/Core.lua"
		},
		{
			"hook_id": "lib/managers/hud/hudmissionbriefing",
			"script_path": "lua/Core.lua"
		},
		{
			"hook_id": "lib/managers/hud/hudblackscreen",
			"script_path": "lua/Core.lua"
		},
		{
			"hook_id": "lib/tweak_data/levelstweakdata",
			"script_path": "lua/Core.lua"
		}
	]
}