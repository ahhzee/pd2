_include("managers/holograms/IntroCinematicDifficulty.lua")

function get_level_data(id)
    local data = tweak_data.levels[id]
    if data and data.anim_data then return data.anim_data
    else 
        data = tweak_data["level_"..id]
        if data and data.anim_data then return data.anim_data
        else return nil end
    end
end

if not IntroCinematicManager then
    IntroCinematicManager = class()

    function IntroCinematicManager:init()
        self._name = "IntroCinematicManager"
    end

    function IntroCinematicManager:connect_mission_briefingui(safe,full)
        self.__safe_ws = safe
        self.__full_ws = full
    end

    function IntroCinematicManager:play(waiting_state,map)
        local map_data = get_level_data(map)
        if not map_data then
            return
        end

        self.__safe_ws:panel():set_visible(false)
        self.__full_ws:panel():set_visible(false)

        managers.overlay_effect:stop_effect(waiting_state._fade_out_id)
        managers.overlay_effect:play_effect(tweak_data.overlay_effects.level_fade_in)

        managers.hud:hide_mission_briefing_hud()
        managers.hud:hide(waiting_state.GUI_SAFERECT)
	    managers.hud:hide(waiting_state.GUI_FULLSCREEN)
        managers.hud:hide(waiting_state.LEVEL_INTRO_GUI)

        waiting_state._cam_unit:set_rotation(map_data.from[2])

        waiting_state._cam_unit:set_position(map_data.from[1])

        waiting_state._camera_data.next_t = 999999999
        waiting_state._cam_unit:camera():start(999999999)
        waiting_state._cam_unit:set_fov(60)

        self.starting_time = -1

        local billboard = nil
        if map_data.marker.align == "billboard_x" then
            billboard = Workspace.BILLBOARD_Y
        elseif map_data.marker.align == "billboard_y" then
            billboard = Workspace.BILLBOARD_X
        elseif map_data.marker.align == "billboard_both" then
            billboard = Workspace.BILLBOARD_BOTH
        end

        local world_pos = map_data.marker.pos
        
        self.hologram = VisualOverhaulCore.managers.holograms:create({
            name = "intro_hologram",
            world_pos = world_pos,
            world_rot = map_data.marker.rot,
            world_size = map_data.marker.size and {map_data.marker.size*2,map_data.marker.size} or {1200,600},
            size = {2000,1000},
            debug = false,
            billboard = billboard,
            update = function (t,dt)
                if self.starting_time == -1 then
                    self.starting_time = t
                end

                local pos_vec = map_data.from[1]
                local rot_vec = map_data.from[2]
                local local_time = t - self.starting_time

                if waiting_state._cam_unit and not self.done then
                    mvector3.lerp(pos_vec,map_data.from[1],map_data.to[1],(local_time*0.001)/map_data.speed)
                    mrotation.slerp(rot_vec,map_data.from[2],map_data.to[2],(local_time*0.001)/map_data.speed)

                    waiting_state._cam_unit:set_position(pos_vec)
                    waiting_state._cam_unit:set_rotation(rot_vec)
                end

                if self._update then
                    self._update(self.starting_time,t,dt)
                end
            end
        })

        -- make this use the name

        local level_data = managers.job:current_level_data()

        self._update = IntroCinematicDifficultyHologram(self.hologram:get_panel(),{
            heist_name = managers.localization:text(level_data.name_id),
            difficulty = managers.job:current_difficulty_stars(),
            size = map_data.marker.size or 500
        })
    end

    function IntroCinematicManager:on_end()
        if self.hologram then
            self.hologram:get_panel():set_visible(false)
            self.hologram:destroy()
            self.__safe_ws:panel():set_visible(true)
	        self.__full_ws:panel():set_visible(true)
            managers.overlay_effect:play_effect(tweak_data.overlay_effects.level_fade_in)
        end
        self.done = true
    end
end


if RequiredScript:lower() == "lib/setups/setup" then
    VisualOverhaulCore.managers.introcinematics = VisualOverhaulCore.managers.introcinematics or IntroCinematicManager:new()
elseif RequiredScript:lower() == "lib/states/ingamewaitingforplayers" then
    Hooks:PostHook(IngameWaitingForPlayersState, "sync_start", "F_"..Idstring("PostHook:sync_start:IntroCinematicManager"):key(), function(self)          
        if VisualOverhaulCore.managers.introcinematics then
            VisualOverhaulCore.managers.introcinematics:play(self,managers.job:current_level_id())
        end
    end)
    
    Hooks:PostHook(IngameWaitingForPlayersState, "at_exit", "F_"..Idstring("PostHook:at_exit:IntroCinematicManager"):key(), function(self)   
        if VisualOverhaulCore.managers.introcinematics then
            VisualOverhaulCore.managers.introcinematics:on_end()
        end
    end)

elseif RequiredScript:lower() == "lib/managers/hud/hudmissionbriefing" then
    Hooks:PostHook(MissionBriefingGui, "init", "F_"..Idstring("PostHook:init:HideDefaultBlackScreen"):key(), function(self)
        if VisualOverhaulCore.managers.introcinematics then
            VisualOverhaulCore.managers.introcinematics:connect_mission_briefingui(self._safe_workspace,self._full_workspace)
        end
    end)
elseif RequiredScript:lower() == "lib/managers/hud/hudblackscreen" then
    Hooks:PostHook(HUDBlackScreen, "set_job_data", "F_"..Idstring("PostHook:set_job_data:HideDefaultBlackScreen"):key(), function(self)
        
        local data = get_level_data(managers.job:current_level_id())
        if data then
            if self._blackscreen_panel:child("job_panel") then
                self._blackscreen_panel:child("job_panel"):set_visible(false)
            elseif self._blackscreen_panel:child("custom_job_panel") then -- Void UI fix
                self._blackscreen_panel:child("custom_job_panel"):set_visible(false)
            end
        else
            if self._blackscreen_panel:child("job_panel") then
                self._blackscreen_panel:child("job_panel"):set_visible(true)
            elseif self._blackscreen_panel:child("custom_job_panel") then -- Void UI fix
                self._blackscreen_panel:child("custom_job_panel"):set_visible(true)
            end
        end

        if self._blackscreen_panel:child("skip_text") and self._blackscreen_panel:child("custom_job_panel") then -- Void UI fix
            self._blackscreen_panel:child("skip_text"):set_center(self._blackscreen_panel:w() / 2, self._blackscreen_panel:h() / 1.05)
        end

    end)
end