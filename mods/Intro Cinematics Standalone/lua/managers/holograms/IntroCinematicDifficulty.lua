_include("utils/easing.lua")
_include("utils/panels.lua")

function clamp(n,l,h)
    l = l or 0
    h = h or 1
    return math.max(l,math.min(h,n))
end

function firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end

function IntroCinematicDifficultyHologram(panel,data)
    local difficulty_panel = panel:panel({
        name = "difficulty_panel",
        h = data.size
    })

    local heist_name = createShadowText(difficulty_panel,0.5,{5,5},{
        name = "heist_name",
        vertical = "top",
        align = "left",
        text = data.heist_name,
        visible = true,
        h = difficulty_panel:h() * 0.5,
        layer = 4,
        color = Color.white,
        font_size = difficulty_panel:h() * 0.5 * VisualOverhaulCore.global.text_size_muliplier,
        font = VisualOverhaulCore.global.font
    })

    local risk_text = createShadowText(difficulty_panel,0.5,{5,5},{
		vertical = "top",
        align = "left",
        y = difficulty_panel:h() * 0.45,
		text = managers.localization:text(tweak_data.difficulty_name_id),
		font = VisualOverhaulCore.global.font,
		font_size = difficulty_panel:h() * 0.3 * VisualOverhaulCore.global.text_size_muliplier,
		color = tweak_data.screen_colors.risk
    })

    local skulls_panel = difficulty_panel:panel({
        name = "skulls_panel",
        y = difficulty_panel:h() * 0.75,
        h = difficulty_panel:h() * 0.2 * VisualOverhaulCore.global.icon_size_muliplier
    })

    local blackscreen_risk_textures = tweak_data.gui.blackscreen_risk_textures

    for i = 1, data.difficulty, 1 do
		local difficulty_name = tweak_data.difficulties[i + 2]
		local texture = blackscreen_risk_textures[difficulty_name] or "guis/textures/pd2/risklevel_blackscreen"
		skulls_panel:bitmap({
            name = "risk_level_"..tostring(i),
			texture = texture,
            color = tweak_data.screen_colors.risk,
            x = skulls_panel:h() * (i-1) - skulls_panel:h()*0.2,
            w = skulls_panel:h(),
            h = skulls_panel:h(),
            alpha = 1
		})
    end

    -- make this more fancy

    return function(s,t,dt)
        risk_text.set_left(math.lerp(-1000,0,(t-s) < 3 and clamp(Easing.easeOutQuart((t - s - 3))) or 1))
        heist_name.set_top(math.lerp(-1000,0,(t-s) < 2 and clamp(Easing.easeOutQuart((t - s - 2))) or 1))
        for i = 1, data.difficulty, 1 do
            local element = skulls_panel:child("risk_level_"..tostring(i))
            if element then
                element:set_top(math.lerp(-element:h(),0,(t-s) < 4.4 and clamp(Easing.easeOutQuart((t - s - 4 - (i*0.1)))) or 1))
            end
        end
    end
end
