Hooks:PostHook(LevelsTweakData, "init", "F_"..Idstring("PostHook:init:LevelsTweakData"):key(), function(self)
    self.gallery.anim_data = {
        from = {
            Vector3(3944,1119,1200),
            Rotation(115,0,0)
        },
        to = {
            Vector3(3284,2254,1200),
            Rotation(100,0,0)
        },
        speed = 10,
        marker = {
            size = 600,
            align = "billboard_x",
            pos = Vector3(2000,7,1000)
        }
    }

    self.framing_frame_1.anim_data = {from={Vector3(3944,1119,1200),Rotation(115,0,0)},to={Vector3(3284,2254,1200),Rotation(130,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(2200,0,1000)}}
    self.branchbank.anim_data = {from={Vector3(-2747,-1012,1100),Rotation(10,0,0)},to={Vector3(45,-774,1100),Rotation(10,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-1994,338,800)}}
    self.cage.anim_data = {from={Vector3(-2016,1097,700),Rotation(220,0,0)},to={Vector3(-3421,-1670,700),Rotation(220,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-1920,-660,500)}}
    self.roberts.anim_data = {from={Vector3(1718,-439,1000),Rotation(120,0,0)},to={Vector3(357,800,1000),Rotation(120,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(404,-920,900)}}
    self.big.anim_data = {from={Vector3(6754,-1253,800),Rotation(50,0,0)},to={Vector3(6754,1089,800),Rotation(50,0,0)},speed=10,marker = {size = 600,pos = Vector3(5249,-395,500)}}
    self.kosugi.anim_data = {from={Vector3(3203,-1453,2000),Rotation(100,0,0)},to={Vector3(1362,1922,2000),Rotation(100,0,0)},speed=10,marker = {size = 600,pos = Vector3(966,-1846,1500)}}
    self.election_day_1.anim_data = {from={Vector3(3808,-1014,1100),Rotation(20,0,0)},to={Vector3(1279,-1904,1100),Rotation(20,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(2038,-202,900)}}
    self.mallcrasher.anim_data = {from={Vector3(-268,-4000,1000),Rotation(5,0,0)},to={Vector3(253,-3702,1000),Rotation(5,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-390,-3359,800)}}
    self.nightclub.anim_data = {from={Vector3(-390,-3359,800),Rotation(225,0,0)},to={Vector3(253,-3702,1000),Rotation(225,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(1232,-4100,600)}}
    self.ukrainian_job.anim_data = {from={Vector3(1305,3000,1000),Rotation(120,0,0)},to={Vector3(-922,3002,1000),Rotation(120,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(190,1382,800)}}
    self.jewelry_store.anim_data = {from={Vector3(1305,3000,1000),Rotation(120,0,0)},to={Vector3(-922,3002,1000),Rotation(120,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(190,1382,800)}}
    self.family.anim_data = {from={Vector3(-668,509,150),Rotation(280,40,0)},to={Vector3(-900,-1428,200),Rotation(280,40,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(23,-246,600)}}
    self.four_stores.anim_data = {from={Vector3(-167,-3393,800),Rotation(250,0,0)},to={Vector3(-2633,-3393,1000),Rotation(250,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(467,-4093,600)}}
    self.firestarter_1.anim_data = {from={Vector3(167,2993,800),Rotation(200,0,0)},to={Vector3(-700,2993,1000),Rotation(200,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(867,1893,600)}}
    self.alex_1.anim_data = {from={Vector3(1010,302,2300),Rotation(280,0,0)},to={Vector3(-1010,-1039,2100),Rotation(280,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(1329,674,1800)}}
    --self.watchdogs_wrapper.anim_data = {}, Doesnt need one
    --self.welcome_to_the_jungle.anim_data = {}, Doesnt work
    self.arm_for.anim_data = {from={Vector3(-1800,-2610,1200),Rotation(280,0,0)},to={Vector3(-4000,-4839,1500),Rotation(280,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-805,-3700,900)}}
    --self.arm_hcm.anim_data = {} There is stuff happening in the cutscene, so this would be weird to use
    self.jolly.anim_data = {from={Vector3(4800,2300,1200),Rotation(70,0,0)},to={Vector3(8000,2839,1500),Rotation(70,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(4000,2300,900)}}
    --self.wwh.anim_data = {} Cinematic intro
    self.pbr.anim_data = {from={Vector3(5679,75,700),Rotation(100,0,0)},to={Vector3(2339,-15,700),Rotation(100,0,0)},speed=10,marker = {size = 500,align = "billboard_x",pos = Vector3(3339,-1215,500)}}

    self.pbr2.anim_data = {from={Vector3(-360,-1000,46000),Rotation(170,40,0)},to={Vector3(-360,-2000,46000),Rotation(170,40,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-400,-3000,46400)}}
    self.mad.anim_data = {from={Vector3(16084,5297,1700),Rotation(60,0,0)},to={Vector3(11815,5688,1700),Rotation(60,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(15084,5297,1600)}}

    self.mex.anim_data = {from={Vector3(-284,-1195,1000),Rotation(300,0,0)},to={Vector3(-312,4485,1000),Rotation(300,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(654,100,800)}}
    self.mex_cooking.anim_data = {
        from = {
            Vector3(3862,-6859,-2000),
            Rotation(100,0,0)
        },
        to = {
            Vector3(3380,-9069,-2000),
            Rotation(140,0,0)
        },
        speed = 10,
        marker = {
            size = 550,
            align = "billboard_x",
            pos = Vector3(2788,-10566,-2200)
        }
    }
    self.tag.anim_data = {from={Vector3(-214,440,1000),Rotation(160,0,0)},to={Vector3(-214,-512,1000),Rotation(160,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-325,-1490,800)}}
    self.spa.anim_data = {from={Vector3(7544,-229,1000),Rotation(250,0,0)},to={Vector3(5544,-229,1100),Rotation(250,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(8144,-529,800)}}
    self.brb.anim_data = {from={Vector3(-614,440,1000),Rotation(160,0,0)},to={Vector3(-614,-512,1000),Rotation(160,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-725,-1490,800)}}
    self.rat.anim_data = {from={Vector3(1010,302,2300),Rotation(280,0,0)},to={Vector3(-1010,-1039,2100),Rotation(280,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(1329,674,1800)}}
    self.pal.anim_data = {from={Vector3(-3138,1960,800),Rotation(50,0,0)},to={Vector3(-4138,1960,850),Rotation(50,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-4221,1602,600)}}
    self.dah.anim_data = {from={Vector3(-3216,1796,1200),Rotation(190,0,0)},to={Vector3(-3959,1257,1250),Rotation(190,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-3023,-48,1000)}}
    self.red2.anim_data = {from={Vector3(-2255,0,1200),Rotation(250,0,0)},to={Vector3(-1000,0,1250),Rotation(250,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-740,-520,1000)}}
    self.peta.anim_data = {
        from = {
                Vector3(-2255,0,1700),
                Rotation(250,0,0)
            },
        to = {
            Vector3(-1000,0,1750),
            Rotation(250,0,0)
        },
        speed = 10,
        marker = {
            size = 500,
            align = "billboard_x",
            pos = Vector3(-740,-520,1500)
        }
    }
    --self.kenaz.anim_data = {} Doesnt work, too little dialogue
    self.glace.anim_data = {from={Vector3(-1359,-14983,7000),Rotation(160,0,0)},to={Vector3(-1359,-12122,7000),Rotation(160,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(-1359,-16122,6700)}}
    --self.run.anim_data = {} can't find a good location
    self.mia_1.anim_data = {from={Vector3(1874,-1224,1000),Rotation(180,0,0)},to={Vector3(640,-410,1000),Rotation(180,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(1439,-2946,800)}}
    self.shoutout_raid.anim_data = {from={Vector3(3203,1453,2300),Rotation(100,0,0)},to={Vector3(1362,2922,2300),Rotation(100,0,0)},speed=10,marker = {size = 600,align = "billboard_x",pos = Vector3(1466,-100,1800)}}
    --self.dark.anim_data = {} Stuff happening
    self.flat.anim_data = {
        from = {
            Vector3(-1382,1068,850),
            Rotation(160,0,0)
        },
        to = {
            Vector3(-1382,1576,1000),
            Rotation(160,0,0)
        },
        speed = 10,
        marker = {
            size = 300,
            align = "billboard_x",
            pos = Vector3(-1582,900,900)
        }
    }

    self.help.anim_data = {
        from = {
            Vector3(-2425,-5037,600),
            Rotation(40,0,60)
        },
        to = {
            Vector3(-3638,-3573,700),
            Rotation(0,0,0)
        },
        speed = 5,
        marker = {
            size = 500,
            align = "billboard_x",
            pos = Vector3(-3578,-3488,500)
        }
    }

    self.rvd1.anim_data = {
        from = {
            Vector3(-2218,-2289,850),
            Rotation(200,0,0)
        },
        to = {
            Vector3(-85,-1096,900),
            Rotation(160,0,0)
        },
        speed = 10,
        marker = {
            size = 600,
            rot = Rotation(90,0,0),
            pos = Vector3(290,-2781,700)
        }
    }

    self.chill_combat.anim_data = {
        from = {
            Vector3(-331,468,200),
            Rotation(250,60,0)
        },
        to = {
            Vector3(734,-13,500),
            Rotation(250,60,0)
        },
        speed = 10,
        marker = {
            size = 400,
            align = "billboard_x",
            pos = Vector3(750,-203,700)
        }
    }

    self.bex.anim_data = {
        from = {
            Vector3(299,2626,600),
            Rotation(160,0,0)
        },
        to = {
            Vector3(226,-116,700),
            Rotation(160,0,0)
        },
        speed = 30,
        marker = {
            size = 500,
            rot = Rotation(90,0,0),
            pos = Vector3(445,1285,500)
        }
    }

    self.mus.anim_data = {
        from = {
            Vector3(-7215.23, -2134.86, 300),
            Rotation(-50,0,0)
        },
        to = {
            Vector3(-5718.16, -560.178, 200),
            Rotation(-80,0,0)
        },
        speed = 10,
        marker = {
            size = 800,
            align = "billboard_x",
            pos = Vector3(-5091.86, 448.21, -100)
        }
    }

    self.sah.anim_data = {
        from = {
            Vector3(-83.2212, -5066.4, 403.3718),
            Rotation(-20,0,0)
        },
        to = {
            Vector3(-19.4314, -2434.1, 830.665),
            Rotation(-20,0,0)
        },
        speed = 30,
        marker = {
            size = 400,
            rot = Rotation(-90,0,0),
            pos = Vector3(-404.515, -3798.07, 300)
        }
    }

    self.bph.anim_data = {
        from = {
            Vector3(-860.294, -1093.93, 500),
            Rotation(140.105, 15.3206, 4.26887e-007)
        },
        to = {
            Vector3(-1996.4, -5761.08, 760),
            Rotation(140.105, 15.3206, 4.26887e-007)
        },
        speed = 30,
        marker = {
            size = 600,
            align = "billboard_x",
            pos = Vector3(-1584.15, -4067.54, 530)
        }
    }

    self.des.anim_data = {
        from = {
            Vector3(-55.109, -1323.97, 800),
            Rotation(160,0,0)
        },
        to = {
            Vector3(-77.6172, -4186.48, 800),
            Rotation(160,0,0)
        },
        speed = 30,
        marker = {
            size = 400,
            align = "billboard_x",
            pos = Vector3(-230, -2500, 700)
        }
    }

    --self.nmh.anim_data = {} No NPC's spawned
    --self.hox_3.anim_data = {} Not useable, loading is slow
    --self.cane.anim_data = {} too little dialogue

    self.friend.anim_data = {
        from = {
            Vector3(6634.48, -3923.26, 619.492),
            Rotation(50,10,0)
        },
        to = {
            Vector3(4461.5, -2881.82, 553.42),
            Rotation(50,10,0)
        },
        speed = 30,
        marker = {
            size = 500,
            rot = Rotation(0,0,0),
            pos = Vector3(5679.13, -3819.56, 420.366)
        }
    }

    self.dinner.anim_data = {
        from = {
            Vector3(-13436.4, 6679.92, 271.73),
            Rotation(-110,10,0)
        },
        to = {
            Vector3(-11399.4, 6649.2, 292.079),
            Rotation(-110,10,0)
        },
        speed = 30,
        marker = {
            size = 400,
            rot = Rotation(-180,0,0),
            pos = Vector3(-12111.4, 6966.01, 284.906)
        }
    }

    self.moon.anim_data = {
        from = {
            Vector3(-3311.36, 752.995, 583.534),
            Rotation(-54.7294, 10, 0)
        },
        to = {
            Vector3(-3426.69, 230.013, 588.294),
            Rotation(-102.68, 10, 0)
        },
        speed = 20,
        marker = {
            size = 400,
            rot = Rotation(-180,0,0),
            pos = Vector3(-2734.99, 580.122, 536.7)
        }
    }

    self.arena.anim_data = {
        from = {
            Vector3(532.512, 285.218, 1023.98),
            Rotation(37.7572, 10, -0)
        },
        to = {
            Vector3(-215.063, 712.934, 1014.75),
            Rotation(-9.84273, 20, -0)
        },
        speed = 10,
        marker = {
            size = 500,
            rot = Rotation(-90,0,0),
            pos = Vector3(-469.914, 1554.71, 1110.31)
        }
    }

    self.born.anim_data = {
        from = {
            Vector3(532.512 - 1000, 285.218 - 3000, 1023.98 - 200),
            Rotation(37.7572, 10, -0)
        },
        to = {
            Vector3(-215.063- 1000, 712.934 - 3000, 1014.75 - 200),
            Rotation(-9.84273, 20, -0)
        },
        speed = 20,
        marker = {
            size = 500,
            rot = Rotation(-90,0,0),
            pos = Vector3(-469.914 - 1000, 1554.71 - 3000, 1110.31 - 200)
        }
    }

    --self.crojob3.anim_data = {} Cinematic intro
    self.crojob2.anim_data = {
        from = {
            Vector3(532.512 - 1000, 285.218 - 3000, 1023.98 - 200),
            Rotation(37.7572, 10, -0)
        },
        to = {
            Vector3(-215.063- 1000, 712.934 - 3000, 1014.75 - 200),
            Rotation(-9.84273, 20, -0)
        },
        speed = 20,
        marker = {
            size = 500,
            rot = Rotation(-90,0,0),
            pos = Vector3(-469.914 - 1000, 1554.71 - 3000, 1110.31 - 200)
        }
    }

    --self.vit.anim_data = {} Cinematic intro

    self.fish.anim_data = {
        from = {
            Vector3(494.122, -5813.4, 86.94),
            Rotation(0, 0, 0)
        },
        to = {
            Vector3(107.158, -4457.45, 50.303),
            Rotation(0, 0, 0)
        },
        speed = 10,
        marker = {
            size = 400,
            rot = Rotation(-90,0,0),
            pos = Vector3(-232.146, -4717.27, -107.11)
        }
    }

    --self.man.anim_data = {} Doesn't wanna work
end)