VisualOverhaulCore.global = {
    font = "thuverx/fonts/roboto_light_large",
    text_size_muliplier = 1,
    icon_size_muliplier = 1
}