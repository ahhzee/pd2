function createShadowText(panel,opacity,offset,x)

    local obj = x

    local org_color = obj.color

    obj.name = (obj.name or "any").."_lower"

    obj.x = (obj.x or 0) + offset[1]
    obj.y = (obj.y or 0) + offset[2]
    obj.color = Color.black:with_alpha(opacity)

    local lower_text = panel:text(obj)

    obj.color = org_color
    obj.x = (obj.x or offset[1]) - offset[1]
    obj.y = (obj.y or offset[2]) - offset[2]

    obj.name = (obj.name or "any").."_upper"

    local upper_text = panel:text(obj)  

    return {
        upper_text = upper_text,
        lower_text = lower_text,
        set_text = function(text)
            upper_text:set_text(text)
            lower_text:set_text(text)
        end,
        set_color = function(color)
            upper_text:set_color(color)
        end,
        set_top = function(x) 
            upper_text:set_top(x)
            lower_text:set_top(x + offset[2])
        end,
        set_font_size = function(x)
            upper_text:set_font_size(x)
            lower_text:set_font_size(x)
        end,
        font_size = function()
            return upper_text:font_size()
        end,
        set_left = function(x) 
            upper_text:set_left(x)
            lower_text:set_left(x + offset[1])
        end
    }
end